<?php


class AnriAboutMe extends WP_Widget
{

    public function __construct()
    {
        parent::__construct('about-me', 'About Me');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'] . $args['before_title'] .$instance['title']. $args['after_title'];
        ?>

        <div class="sidebar-widget__about-me">
            <div class="sidebar-widget__about-me-image">
                <img src="img/about-me.jpg" alt="About Me">
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis commodo ullamcorper.</p>
        </div>


        <?php
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        ?>

        <label for="<?php echo $this->get_field_id('title');  ?>">Title</label>
        <input value="<?php echo $instance['title'];  ?>" id="<?php echo $this->get_field_id('title');  ?>"  name="<?php echo $this->get_field_name('title');  ?>" class="widefat" type="text">

        
    <?php }

}