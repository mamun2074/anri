<?php


function anry_setup()
{


    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ));

    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', array(
        'width' => 250,
        'height' => 250,
        'flex-width' => true,
    ));


//    Menu supportn

    add_theme_support('menus');

//    Widgets

    add_theme_support('widgets');
}

add_action('after_setup_theme', 'anry_setup');


//Widgets support area

function anry_footer_widget()
{

    register_sidebar(array(
        'name'  =>'Right sidebar',
        'id'    =>'right_sidebar',
        'after_widget'=>'</div>',
        'before_widget'=>'<div class="sidebar-widget">',

        'after_title'  =>'</h3>',
        'before_title'=>'<h3>',

    ));


}
add_action('widgets_init','anry_footer_widget');




function anry_style()
{

    wp_enqueue_style('anry_bootsrap', get_theme_file_uri('/assets/css/bootstrap.min.css'));
    wp_enqueue_style('anry_main_style', get_theme_file_uri('/assets/css/style.min.css'));

    wp_enqueue_style('stylesheet', get_stylesheet_uri());


    wp_enqueue_script('jquery');

    wp_enqueue_script('script', get_theme_file_uri('/assets/js/script.js'), array('jquery'), true);


}

add_action('wp_enqueue_scripts', 'anry_style');


// Custom pagination function

function anri_blog_paginatio()
{

    global $wp_query;

    $newqueru = new WP_Query(array(
        'post_type' => 'post',
    ));

//   $mmm=$newqueru->get('paged');
//   echo max(1,$mmm);
//
//   die();


    $big = 999999999; // need an unlikely integer

    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $newqueru->max_num_pages,
        'prev_text' => __(' Prev Page'),
        'next_text' => __('Next Page'),
        'type' => 'array',

    ));
    if (is_array($pages)) {
        $paged = (get_query_var('paged') == 0) ? 1 : get_query_var('paged');

        echo '<nav class="blog-pagination"><ul class="blog-pagination__items">';

        foreach ($pages as $page) {
            echo "<li class='blog-pagination__item'>$page</li>";
        }
        echo '</ul></nav>';
    }
}


//Register Widget

function anri_custom_widgets(){
//    About me
    register_widget('AnriAboutMe');
}
add_action('widgets_init','anri_custom_widgets');

include_once 'inc/Widgets/anri.aboutus.php';












